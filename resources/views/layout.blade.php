 <!DOCTYPE html>
<html lang="en">
<head>

    @yield('headers')
    
</head>
<body>
  <header>
        <div class="contenedor_navbar">
            <div class="contenedor_animada">
                <ul>
                    <li>
                        <h1 class="h1">US$3 </h1> DESCUENTO DE SU PRIMERA ORDEN
                    </li>
                    <li>
                        <h1 class="h1">US$3 </h1> DESCUENTA DE ROPA DE VERANO
                    </li>
                </ul>
            </div>
            <a href="{{route('index')}}" class="navbar_logo">
                <img src="img/logo.png" alt="Logo">
            </a>
            <input type="checkbox" id="menu_navbar">
            <label for="menu_navbar">
                <img src="img/menu.png" alt="menu">
            </label>
            <nav class="menu_nav">
                <ul class="menu_principal">
                    <li class="menu_lista">
                        <div class="container_1">
                            <form action="" autocomplete="on">
                                <input id="search" name="search" type="text" placeholder="Buscar..." class="search">
                                <input type="submit" id="search_submit" value="Buscar">
                            </form>
                        </div>
                    </li>
                    <li class="menu_lista">
                        <a href="{{ route('prendas')}}">Prendas <img src="img/arrow.png" alt="arrow"></a>
                    </li>
                    <li class="menu_lista sub_cuenta">
                        <a href="#" class="cuenta">Cuenta <span class="fas fa-sort-down"></span></a>
                        <ul class="sub_menu">
                            @guest
                            <li class="menu_lista">
                                <a href="{{route('login')}}">Iniciar Sesion <img src="img/arrow.png" alt="arrow"></a>
                            </li>
                             <li class="menu_lista">
                                <a href="{{route('compras')}}">Compras <img src="img/arrow.png" alt="arrow"></a>
                            </li>
                            <li class="menu_lista">
                                <a href="{{route('resenas.index')}}">Reseñas <img src="img/arrow.png" alt="arrow"></a>
                            </li>
                            @else

                            <li class="menu_lista">
                                <a href="#"><?php echo explode(" ",Auth::user()->name)[0]; ?> <img src="img/arrow.png" alt="arrow"></a>
                            </li>
                            <li class="menu_lista">
                                <a href="{{route('compras')}}">Compras <img src="img/arrow.png" alt="arrow"></a>
                            </li>
                            <li class="menu_lista">
                                <a href="{{route('resenas.index')}}">Reseñas <img src="img/arrow.png" alt="arrow"></a>
                            </li>
                            <li class="menu_lista">
                                <a  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Salir
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </li>

                            @if(Auth::user()->role=='admin')

                            <li class="menu_lista">
                                <a href="{{route('prendas')}}">Administrar <img src="img/arrow.png" alt="arrow"></a>
                            </li>


                            @endif


                            @endguest
                        </ul>
                    </li>
                    <li class="menu_lista_2">
                        <a href="{{route('carrito')}}">Carrito de Compra <img src="img/arrow.png" alt="arrow"></a>
                    </li>
                    <li class="menu_lista_3">
                        <a href="{{route('carrito')}}"><span class="fas fa-shopping-cart"></span></a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>



            @yield('main')



                <footer>
            <div class="politica">
                    <h1>Politicas de la Tienda</h1> 
                    <br>
                    <article>
                        <ul>
                            <li>Términos y condiciones</li>
                            <li>Horario de Atención</li>
                            <li>Libro de Reclamos</li>
                            <li>Políticas de entrega , garantía y devolución</li>
                        </ul>
                    </article> 
                    <br>   
            </div>
            <div class="tienda">
                    <h1>Información de la Tienda</h1> 
                    <br>
                    <article>
                        <ul>
                            <li>Tienda   Elimar</li>
                            <li>Llámanos ahora al: 951 545 421</li>
                            <li>Email: elimar@gmail.com</li>
                        </ul> 
                    </article>
                    <br>
            </div>
            <div class="redes">
                <h1>Redes Sociales</h1> 
                <br>
                <article>
                    <ul>
                        <li><img src="img/fooder/facebook.png" alt="facebook" class="redes_sociales"> Facebook</li>
                        <li><img src="img/fooder/instagram.png" alt="instagram" class="redes_sociales">Instagram</li>
                        <li><img src="img/fooder/twitter.png" alt="twitter" class="redes_sociales">Twiter</li>
                        <li><img src="img/fooder/whatsapp.png" alt="whatsapp" class="redes_sociales">Whatsapp</li>
                    </ul>     
                </article>
                <br>     
            </div>
            <div class="copyright">
                    <p class="copyroght">
                            © ELIMAR - Todos los Derechos Reservados 2019
                    </p>
            </div>
        </footer>

        @yield('fieldjs')
</body>
</html>