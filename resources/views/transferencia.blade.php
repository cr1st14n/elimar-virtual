@extends('layout')





@section('headers')


  <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transferencia Bancaria</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar.css">

    <link rel="stylesheet" href="css/pago/bancaria.css">

    <script src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="estilos/all.min.css">
    <script src="js/all.min.js"></script>




@endsection


@section('main')


<main>
        <section>
            <div>
                <h1 class="titulo">Transferencia Bancaria</h1>
                <div class="contenedor_general">
                        <div class="contenedor">
                                <form action="">
                                    <div class="transferencia_label">
                                            <label>Nombre del Titular</label>
                                            <input type="text" class="formulario">
                                    </div>
                                    <div class="transferencia_label">
                                            <label>Apellido</label>
                                            <input type="text" class="formulario">
                                    </div>
                                    <div class="transferencia_label">
                                        <label>Distrito</label>
                                        <input type="text" class="formulario">
                                    </div>
                                    <div class="transferencia_label">
                                            <label>Dirección</label>
                                            <input type="text" class="formulario">
                                    </div>
                                    <div class="contenedor_telefono">
                                            <div class="transferencia_label">
                                                    <label>Telefono</label>
                                                    <input type="text" class="formulario">
                                            </div>
                                            <div class="transferencia_label">
                                                    <label>Telefono Opc.</label>
                                                    <input type="text" class="formulario">
                                            </div>
                                    </div>
                                </form>
                                <button>Realizar Compra</button>
                                <p>Cuenta a depositar:lasjljadsljflasjl</p>
                                <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500</p>
                            </div>
                            <div class="des_pago">
                                    <div class="fases_img">
                                            <h1 class="fases">Fase1</h1>
                                        <img src="img/tipo_compra/banco.png" alt="banco" class="banco">
                                        
                                        <p class="fases_1">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor.</p>
                                    </div>
                                    <div class="fases_img">
                                            <h1 class="fases">Fase2</h1>
                                            <img src="img/tipo_compra/boleta.jpg" alt="boleto" class="boleto">
                                            
                                            <p class="fases_1">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor.</p>
                                    </div>
                                    <div class="fases_img">
                                            <h1 class="fases">Fase3</h1>
                                            <img src="img/tipo_compra/gmail.jpg" alt="gamil" class="gmail">
                                            
                                            <p class="fases_1">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor.</p>
                                    </div>
                                </div>
                </div>
               
                
                
            </div>
        </section>
    </main>


@endsection
