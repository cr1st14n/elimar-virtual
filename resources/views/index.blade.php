
@extends('layout')

@section('headers')

 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Elimar</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar.css">

    <link rel="stylesheet" href="css/css_home/informacion.css">
    <link rel="stylesheet" href="css/css_home/ropa_favorita.css">
    <link rel="stylesheet" href="css/css_home/categorias.css">

    <link rel="stylesheet" href="css/css_home/banner.css">
    <link rel="stylesheet" href="css/all.min.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/main.js"></script>


@endsection



@section('main')

 <main>
        <section class="banner_general">
            <div class="slidershow">
                <ul class="slider">
                    <li>
                        <img src="img/filder/1.jpg" alt="" class="banner">
                    </li>
                    <li>
                        <img src="img/filder/2.jpg" alt="" class="banner">
                    </li>
                    <li>
                        <img src="img/filder/banner10.jpg" alt="" class="banner">
                    </li>
                    <li>
                        <img src="img/filder/4.jpg" alt="" class="banner">
                    </li>
                </ul>
                <ol class="paginacion">
                </ol>
            </div>
            <div class="banner_2">
                <img src="img/Banner/1.jpg" alt="banner_pequeño">
            </div>
        </section>
        <section class="categorias">
            <div class="contenedor_cate">
                <div class="conteniener_cate">
                    <img src="img/categoria_1/blouse-with-lace.png" alt="blusa">
                    <br>
                    Blusa
                    <div class="enlace_cate">
                        <button>Ver</button>
                    </div>
                </div>
                <div class="conteniener_cate">
                    <img src="img/categoria_1/coat (1).png" alt="saco">
                    <br>
                    Sacos
                    <div class="enlace_cate">
                        <button>Ver</button>
                    </div>
                </div>
                <div class="conteniener_cate">
                    <img src="img/categoria_1/dress (1).png" alt="vestido">
                    <br>
                    Vestidos
                    <div class="enlace_cate">
                        <button>Ver</button>
                    </div>
                </div>
                <div class="conteniener_cate">
                    <img src="img/categoria_1/jeans (1).png" alt="pantalon">
                    <br>
                    Pantalon
                    <div class="enlace_cate">
                        <button>Ver</button>
                    </div>
                </div>
            </div>
            <div class="caja_categoria">
                <ul>
                    <li> <a href="galleria.html">Blusa</a></li>
                    <li> <a href="galleria.html">Saco</a></li>
                    <li> <a href="galleria.html">Vestido</a></li>
                    <li> <a href="galleria.html">Patalon</a></li>
                </ul>
            </div>
        </section>
        <section class="ropas_favorita">
            <div class="container_ropa">

                @forelse($collecion as $listar)


                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="http://creaciones.test/galeria/{{ $listar->tipo_id}}">
                            <img src="storage/{{ $listar->image[0]->urlimg}}" alt="Ropa" class="ropa_moda">
                            
                        </a>
                    </div>
                    <h1>{{$listar->nombre}}</h1>
                    <h2>S./ {{$listar->precio}}</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>

                @empty


                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="descripcion_prenda.html">
                            <img src="img/Ropa/f_0072002-morgan-onroll-2.jpg" alt="Ropa" class="ropa_moda">
                            
                        </a>
                    </div>
                    <h1>cuello bebe</h1>
                    <h2>S./ 500.99</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>
                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="descripcion_prenda.html">
                            <img src="img/Ropa/falda-corta-lisa-cinturilla-elastica-negro.jpg" alt="Ropa" class="ropa_moda">
                            
                        </a>
                    </div>
                    <h1>cuello bebe</h1>
                    <h2>S./ 500.99</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>
                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="descripcion_prenda.html">
                            <img src="img/Ropa/michael_lauren_camiseta_hyde_mujer_harina_de_avena_tops_modelos_clsicos_ropa_mujer_4r9t42ghe-500x500.jpg"
                                alt="Ropa" class="ropa_moda">
                                
                        </a>
                    </div>
                    <h1>cuello bebe</h1>
                    <h2>S./ 500.99</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>
                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="descripcion_prenda.html">
                            <img src="img/Ropa/82e3a9c6946fb75f199826b7913551ec.image.448x500.jpg" alt="Ropa" class="ropa_moda">
                            
                        </a>
                    </div>
                    <h1>cuello bebe</h1>
                    <h2>S./ 500.99</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>
                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="descripcion_prenda.html">
                            <img src="img/Ropa/ffe501f00d5666dd478f4a219159fcf6.jpg" alt="Ropa" class="ropa_moda">
                            
                        </a>
                    </div>
                    <h1>cuello bebe</h1>
                    <h2>S./ 500.99</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>
                <div class="caja_ropa">
                    <div class="ropa_galleria">
                        <a href="descripcion_prenda.html">
                            <img src="img/Ropa/10299-faldaamsanegro_1.jpg" alt="Ropa" class="ropa_moda">
                            
                        </a>
                    </div>
                    <h1>cuello bebe</h1>
                    <h2>S./ 500.99</h2>
                    <a href="descripcion_prenda.html" class="ver_mas">Ver Mas</a>
                </div>


                @endforelse


                
            </div>
        </section>
        <section class="informacion">
            <h1>
                ROPA ONLINE PARA MUJER
            </h1>
            <hr>
            <p>
                En Elimar encontrarás la mejor selección de ropa online sin moverte de casa. Desde nuestra tienda
                online podrás comprar las mejores marcas de moda como son Vila Clothes, Maison Scotch, Only, Ichi, Vero
                Moda, Nosy May o Morgan de Toi entre otras. Míralas todas en el apartado de marcas de mujer. Descubre
                navegando sección por sección las prendas con más estilo para crear todo tipo de outfits.
            </p>
            <p>
                Inspírate en los looks que llevan las mejores bloggers de moda como Lovely Pepa, Madame de Rosa, Bala
                moda, Trendy Taste o Mi aventura con la moda, entre muchas otras. Si lo que te gusta esestar a la
                última y enterarte de lo que se lleva y cómo se lleva, sólo tienes que hacer una cosa: consultar
                nuestros apartados de tendencias, bloggers y looks. Aprovecha las tendencias que te proponemos de ropa
                ibicenca, estampados florales o topos, colores pastel, camisetas con mensaje o de estilo boho chic para
                encontrar esa prenda que tanto estás buscando. ¿Ya lo has hecho? Genial, ya puedes hacerte con las
                prendas del momento que más te favorezcan y que te hagan sentir radiante. Abrigos de pelo, pantalones
                de campana, chaquetas de ante, jerséis de punto, sudaderas mujer, vestidos ibicencos, faldas étnicas
                son sólo algunos ejemplos entre las muchas opciones que te proponemos.
            </p>
        </section>
    </main>
@endsection

