@extends('layout')


@section('headers')


 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar.css">

    <link rel="stylesheet" href="css/login/login.css">

    <link rel="stylesheet" href="css/all.min.css">
    <script src="js/jquery.min.js"></script> 
    <script src="js/all.min.js"></script>


@endsection


@section('main')
<main>
        <div class="conenedor_login">
                <div class="Login">
                        <h1 class="titulo">Iniciar Sesión</h1>
                        <form method="POST" action="{{ route('login') }}" class="formulario_usuario">

                            @csrf

                            <label>E-mail</label>
                            <br>
                            <input type="text" name="email" value="{{ old('email') }}" required>
                            <br>
                            <label>Contraseña</label>
                            <br>
                            <input type="password" name="password" required>
                            <br>
                            <button type="submit">Aceptar</button>
                            <br>
                            @if ($errors->has('email'))
                            <a href="{{route('password.request')}}"><p> ¿Olvidaste tu contraseña?</p></a>
                            @endif
                        </form>
                    </div>
                
                    <div class="registrate">
                        <h1 class="titulo">Resgitrase</h1>
                        <form method="POST" action="{{ route('register') }}" class="formulario_usuario">
                            @csrf
                            <label>Nombre</label>
                            <br>
                            <input type="text" name="name" value="{{ old('name') }}" required autofocus>
                            <br>
                            <label>Gmail</label>
                            <br>
                            <input type="text" name="email" value="{{ old('email') }}" required>
                            <br>
                            <label>Contraseña</label>
                            <br>
                            <input type="password" name="password" required>
                            <br>
                            <label>Repita contraseña</label>
                            <br>
                            <input type="password" name="password_confirmation" required>
                            <br>
                        <button>Aceptar</button>
                        </form>
                    </div>
        </div>
    </main>


@endsection
