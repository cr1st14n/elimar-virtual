@extends('layout')





@section('headers')

   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/navbar.css">

    <link rel="stylesheet" href="css/comentario_prenda/perfil_usuario.css">
    <link rel="stylesheet" href="css/comentario_prenda/estado_pedido.css">
    <link rel="stylesheet" href="css/comentario_prenda/modal_estado.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/main.js"></script>


@endsection


@section('main')
  <main>
        <section>
            <div class="usuario_resena">
                <div class="perfil_usuario">
                    <h1>Perfil de usuario</h1>
                    <div>
                        <h2>Usuario:</h2>
                        <h3>Cristian alvarez</h3>
                    </div>
                    <div>
                        <h2>Correo:</h2>
                        <h3>Cristian@gmail.com</h3>
                    </div>
                    <div class="contenedor_bton">
                        <div>
                            <a href="{{route('compras')}}">Comprar</a>
                        </div>
                        <div>
                            <a href="{{route('resenas.index')}}">Reseña</a>
                        </div>
                    </div>
                </div>
                <div class="contenedor_estados">
                    @csrf

                   <div>
                    @forelse($arrayCompras as $listarCompras)
                    <div class="caja_estado">
                        <div class="caja_info">
                            <h1>Estado</h1>
                            <h2>{{$listarCompras->estado->estado}}</h2>
                        </div>
                        <div class="caja_info">
                            <h1>Fecha de Compra</h1>
                            <h2>{{$listarCompras->created_at}}</h2>
                        </div>
                        <div class="caja_info">
                            <h1>Fecha de Entrega</h1>
                            <h2>10/25/26</h2>
                        </div>
                        <div class="caja_info">
                            <h1>Costo Envio</h1>
                            <h2>S/{{$listarCompras->cenv}}</h2>
                        </div>
                        <div class="caja_info">
                            <h1>Formato de Pago</h1>
                            <h2>{{$listarCompras->forma_pago->fpago}}</h2>
                        </div>
                        <div class="caja_info">
                            <h1>Pago Total</h1>
                            <h2>S/{{$listarCompras->totalp}}</h2>
                        </div>
                        <a href="#" key={{$listarCompras->id}} class="comprasDetalle">Ver Detalles</a>
                        @csrf 
                    </div>
                    @empty
                      <div class="caja_estado">
                       NO HAY COMPRAS
                    </div>
                    

                    @endforelse
                    </div>
                    <div>{{$arrayCompras->links() }}</div>
                    
                </div>
                
            </div>
        </section>
        <section class="modal_compra" id="view_modal_compra">
             <a href="#" class="close">X</a>
                <div class="contenedor_galleria">
                       
                        <div style="margin-bottom:15px;border-bottom: 5px solid #B4B4B4 ;padding-bottom:15px;">
                    
                        <div class="contenedor_ropa">
                            <div class="caja_ropa">
                                <img src="img/Ropa/691043_899A08_portrait_HD_1.jpg" alt="">
                            </div>
                            <div class="caja_text">
                                <div class="texto_informacion">
                                    <div class="texto_info">
                                        <h3>Diseño</h3>
                                        <p>Sybilla</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Color</h3>
                                        <p>Plomo</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Talla</h3>
                                        <p>xl</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Precio</h3>
                                        <p>S./599.99</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>cantidad</h3>
                                        <p>25</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Total</h3>
                                        <p>S./599.99</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="comentario_resana">
                                <h2>
                                        Reseña y/o Comentario
                                    </h2>
                                    <form action="">
                                        <textarea name="comentario" id="textarea"></textarea>
                                        <button>Agregar</button>
                                    </form>
                        </div>
                  </div>
                   <div style="margin-bottom:20px;">
                    
                        <div class="contenedor_ropa">
                            <div class="caja_ropa">
                                <img src="img/Ropa/691043_899A08_portrait_HD_1.jpg" alt="">
                            </div>
                            <div class="caja_text">
                                <div class="texto_informacion">
                                    <div class="texto_info">
                                        <h3>Diseño</h3>
                                        <p>Sybilla</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Color</h3>
                                        <p>Plomo</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Talla</h3>
                                        <p>xl</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Precio</h3>
                                        <p>S./599.99</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>cantidad</h3>
                                        <p>25</p>
                                    </div>
                                    <div class="texto_info">
                                        <h3>Total</h3>
                                        <p>S./599.99</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="comentario_resana">
                                <h2>
                                        Reseña y/o Comentario
                                    </h2>
                                    <form action="">
                                        <textarea name="comentario" id="textarea"></textarea>
                                        <button>Agregar</button>
                                    </form>
                        </div>
                  </div>
            </div>
        </section>
    </main>

@endsection


@section('fieldjs')

<script src="js/estado/model.js"></script>

@endsection