@extends('layoutShow')





@section('headers')

 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Galleria</title>

    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/footer.css">
    <!-- <link rel="stylesheet" href="estilos/estilos.css"> -->
    <link rel="stylesheet" href="../css/navbar.css">

    <link rel="stylesheet" href="../css/galleria/galleria.css">
    <link rel="stylesheet" href="../css/galleria/select.css">

    <link rel="stylesheet" href="../css/all.min.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/all.min.js"></script>
    <script src="../js/main.js"></script>




@endsection



@section('main')


  <main>
        <section class="contenedor_btn">
            <div class="caja_btn">
                <ul>
                    @forelse($tipos as $listar)
                    <li class="lista_btn"><a href="http://190.239.159.45/galeria/{{$listar->id}}">{{ $listar->tipo}}</a></li>
                    {{-- <li class="lista_btn"><a href="#">Vestidos</a></li>
                    <li class="lista_btn"><a href="#">Faldas</a></li>
                    <li class="lista_btn"><a href="#">Sacos</a></li>
                    <li class="lista_btn"><a href="#">Blusas</a></li>
                    <li class="lista_btn"><a href="#">Todos</a></li> --}}
                    @empty




                    @endforelse

                    <li class="lista_btn"><a href="http://190.239.159.45/galeria">Todos</a></li> 
                </ul>
            </div>
        </section>
        <section class="contenedor_obciones">
                
                <input type="search" placeholder="Buscar">
            <div class="contenedor_select">
                <div class="list_prendas">
                    <select name="prendas" id="prendas">
                        <option value="prendas">Prendas</option>
                        <option value="blusas">Blusas</option>
                        <option value="pantalon">Pantalon</option>
                        <option value="vestido">Vestido</option>
                        <option value="Saco">Saco</option>
                    </select>
                </div>
                <div class="list_precios">
                        <select name="precios" id="Preciso">
                                <option value="Preciso">Precios</option>
                                <option value="pantalon">Pantalon</option>
                                <option value="vestido">Vestido</option>
                            </select>
                </div>
            </div>
        </section>
        <section>
            <div class="contenedor_galleria">



                @forelse($collecion as $listar)

                 <div class="galleria_ropa">
                            <div class="contenedor_img">
                                <a href="http://190.239.159.45/prendaDescripcion/{{$listar->id}}/{{$listar->image[0]->color}}"> 
                                    <img src="../storage/{{$listar->image[0]->urlimg}}" alt="">
                                    
                                </a>
                                <div class="flotante_galleria">
                                    <a href="#">Ver Más</a>
                                </div>
                            </div>
                            <h1>{{$listar->nombre}}</h1>
                            <h2>S./{{$listar->precio}}</h2>
                    </div>

                @empty

                <div class="galleria_ropa">


                    NO SE ENCONTRARON PRENDAS DE ESE TIPO
                            {{-- <div class="contenedor_img">
                                <a href="descripcion_prenda.html">
                                    <img src="img/Ropa/falda-de-corte-lapiz-pata-de-gallo-elastico-negro-crudo.jpg" alt="#">
                                    
                                </a>
                                </a>
                                <div class="flotante_galleria">
                                    <a href="#">Ver Más</a>
                                </div>
                            </div>
                            <h1>Cuello de Bebe</h1>
                            <h2>S./66.99</h2> --}}
                    </div>
                   


                @endforelse



                   
                    
            </div> 
        </section>
    </main>

@endsection