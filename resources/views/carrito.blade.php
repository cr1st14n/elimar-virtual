@extends('layout')

@section('headers')

<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carrito de Compra</title>
    <link rel="stylesheet" href="css/shopping_card/info.css">
    <link rel="stylesheet" href="css/shopping_card/bola_compra.css">

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar.css">
    <link rel="stylesheet" href="css/shopping_card/modal_compra.css">

    <link rel="stylesheet" href="css/all.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/all.min.js"></script>



@endsection


@section('main')


<main>
        <section class="info">
            <div class="contenedor_info">
                <div class="repositorio_info">
                    <img src="img/categoria_1/logistics-delivery-truck-in-movement copia.png" alt="">
                    <br>
                    Consulta de Envio
                </div>
                <div class="repositorio_info">
                    <img src="img/categoria_1/money-coin-or-button-with-dollar-sign-in-black-circle copia.png" alt="">
                    <br>
                    Obciones de Pago
                </div>
                <div class="repositorio_info">
                    <img src="img/categoria_1/whatsapp2 copia.png" alt="">
                    <br>
                    Dudas
                </div>
            </div>
        </section>
        <section class="carrito_compra">
            <h1>Bolsa de Compras</h1>
            <div class="contenedor_general">
                <div id="contenedor_galleria" class="contenedor_galleria">


                    CARGANDO...
                    {{-- <div class="contenedor_ropa">
                        <div class="caja_ropa">
                            <img src="img/Ropa/691043_899A08_portrait_HD_1.jpg" alt="">
                        </div>
                        <div class="caja_text">
                            <div class="texto_informacion">
                                <div class="texto_info">
                                    <h3>Diseño</h3>
                                    <p>Sybilla</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Color</h3>
                                    <p>Plomo</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Talla</h3>
                                    <p>xl</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Precio</h3>
                                    <p>S./599.99</p>
                                </div>
                                <div class="texto_info">
                                    <h3>cantidad</h3>
                                    <input type="text">
                                </div>
                                <div class="texto_info">
                                    <h3>Total</h3>
                                    <p>S./599.99</p>
                                </div>
                            </div>
                            <button>Eliminar</button>
                        </div>
                    </div>
                    <div class="contenedor_ropa">
                        <div class="caja_ropa">
                            <img src="img/Ropa/691043_899A08_portrait_HD_1.jpg" alt="">
                        </div>
                        <div class="caja_text">
                            <div class="texto_informacion">
                                <div class="texto_info">
                                    <h3>Diseño</h3>
                                    <p>Sybilla</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Color</h3>
                                    <p>Plomo</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Talla</h3>
                                    <p>xl</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Precio</h3>
                                    <p>S./599.99</p>
                                </div>
                                <div class="texto_info">
                                    <h3>cantidad</h3>
                                    <input type="text">
                                </div>
                                <div class="texto_info">
                                    <h3>Total</h3>
                                    <p>S./599.99</p>
                                </div>
                            </div>
                            <button>Eliminar</button>
                        </div>
                    </div>
                    <div class="contenedor_ropa">
                        <div class="caja_ropa">
                            <img src="img/Ropa/691043_899A08_portrait_HD_1.jpg" alt="">
                        </div>
                        <div class="caja_text">
                            <div class="texto_informacion">
                                <div class="texto_info">
                                    <h3>Diseño</h3>
                                    <p>Sybilla</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Color</h3>
                                    <p>Plomo</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Talla</h3>
                                    <p>xl</p>
                                </div>
                                <div class="texto_info">
                                    <h3>Precio</h3>
                                    <p>S./599.99</p>
                                </div>
                                <div class="texto_info">
                                    <h3>cantidad</h3>
                                    <input type="text">
                                </div>
                                <div class="texto_info">
                                    <h3>Total</h3>
                                    <p>S./599.99</p>
                                </div>
                            </div>
                            <button>Eliminar</button>
                        </div>
                    </div> --}}

                </div>
                <div class="contenedor_compra" id="contenedor_compra">
                   {{--  <div class="repositorio_compra">
                        <h2>Resumen de Tu Compra</h2>
                        <div class="caja_compra">
                            <table>
                                <tr>
                                    <td>Subt Total</td>
                                    <td id="subtotal">S/.0.00</td>
                                </tr>
                                <tr>
                                    <td>Costo de Envio</td>
                                    <td id="costoEnvio" coste='5'>S/0.00</td>
                                </tr>
                                <tr>
                                    <td>Total de Pedido</td>
                                    <td id="montoTotal">S./0.00</td>
                                </tr>
                            </table>
                        </div>
                        <button id="open_modal_compra">Comprar</button>
            
                    </div>
                    <section class="modal_compra" id="view_modal_compra">
                        <div class="compra_modal">
                            <a href="#" class="close">X</a>
                            <h1>Elija su Forma de Pago</h1>
                            <div><input type="radio" name="fo_pago" value="tarjeta">

                                <img src="img/tipo_compra/shopping-store.png" alt="">
                                <h2>Tarjeta</h2>
                                <br>
                                <br>
                                <p>PUEDE REALIZAR SUS PAGOS CON TARJETA VISA, MASTERCARD, ENTRE OTROS,
                                    SOLO LLENE EL FORMULARIO CON LOS DATOS DE SU TARJETA Y EL PRODUCTO
                                    LE ESTAR LLEGANDO DENTRO DE LAS 24HORAS</p>
                            </div>

                            <div>
                                <input type="radio" name="fo_pago" value="transferencia">

                                <img src="img/tipo_compra/bank.png" alt="">
                                <h2>Transferencia bancaria</h2>
                                <br>
                                <br>
                                <p>PUEDE REALIZAR SUS PAGOS CON TRANSFERENCIA BANCARIAS,
                                    SOLO LLENE EL FORMULARIO CON LOS DATOS DE SU TARJETA Y EL PRODUCTO
                                    LE ESTAR LLEGANDO DENTRO DE LAS 24HORAS</p>
                            </div>

                            <div>
                                <input type="radio" name="fo_pago" value="contra_entrega">

                                <img src="img/tipo_compra/buy2.png" alt="">
                                <h2>Contra entraga</h2>
                                <br>
                                <br>

                                <p>PUEDE REALIZAR SUS PAGOS CON TARJETA VISA, MASTERCARD, ENTRE OTROS,
                                    SOLO LLENE EL FORMULARIO CON LOS DATOS DE SU TARJETA Y EL PRODUCTO
                                    LE ESTAR LLEGANDO DENTRO DE LAS 24HORAS</p>
                            </div>

                            <button>Continuar</button>
                        </div>
                    </section> --}}
                </div>
            </div>
        </section>
        <section class="pago">
            <h1>Opciones de compra:</h1>
            <div class="repositorio_pago">
                <ul>
                    <li class="vista"><img src="img/vista/visa1.png" alt=""></li>
                    <li class="vista"><img src="img/vista/visa2.png" alt=""></li>
                    <li class="vista"><img src="img/vista/visa3.png" alt=""></li>
                </ul>
            </div>
        </section>
    </main>

@endsection


@section('fieldjs')


<script src="js/shoping_card/model.js"></script>


@endsection


