@extends('layout')





@section('headers')

<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pago Tarjeta</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar.css">

    <link rel="stylesheet" href="css/pago/tarjeta.css">

    <link rel="stylesheet" href="css/all.min.css">
    <script src="js/jquery.min.js"></script> 
    <script src="js/all.min.js"></script>


@endsection



@section('main')


<main>
                        <section>
                                        <h1 class="titulo">Tarjeta Vista</h1>
                                        <div>
                                            <form action="">
                                                <div class="contenedor_formulario">
                                                    <div class="formulario">
                                                        <div>
                                                                <label>Nombre titular</label>
                                                                <input type="text">
                                                        </div>
                                                        <div>
                                                                <label>N° de Tarjeta</label>              
                                                                <input type="text">
                                                        </div>
                                                        <div>
                                                                        <label>Codigo de Seguridad</label>              
                                                                        <input type="text">
                                                                </div>
                                                        <div class="contenedor_fecha">
                                                                <div class="fecha">
                                                                        <label>Fecha de Vencimiento</label>
                                                                        <input type="time">
                                                                </div>
                                                                <div class="cvr">
                                                                        <label>Cvc</label>
                                                                        <input type="text">
                                                                </div>
                                                        </div>
                                                        
                                                        <div class="contenedor_telefono">
                                                                <div>
                                                                        <label>Pais</label>
                                                                        <input type="text">
                                                                </div>
                                                                <div>
                                                                        <label>Codigo Postal</label>
                                                                        <input type="text">
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="formulario">
                                                        <div>
                                                                <label>Distrito</label>
                                                                <input type="text">
                                                        </div>
                                                        <div>
                                                                <label>Direccion</label>
                                                                <input type="text">
                                                        </div>
                                                        <div class="contenedor_telefono">
                                                                <div>
                                                                        <label>Telefono</label>
                                                                        <input type="text">
                                                                </div>
                                                                <div>
                                                                        <label>Telefono Opc.</label>
                                                                        <input type="text">
                                                                </div>  
                                                        </div>
                                                        <div class="img_tarjate">
                                                                <img src="img/credit-card2.png" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <button>Realizar Compra</button>
                                            </form>
                                        </div>
                                        <div>
                                            <p class="info_tajeta">
                                                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 15
                                                </p>
                                        </div>
                                    </section>
        </main>


@endsection