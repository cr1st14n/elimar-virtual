@extends('layoutDescripcion')



@section('headers')


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Descripcion del producto</title>
    <link rel="stylesheet" href="../../css/css_descripcion/descripcion.css">
    <link rel="stylesheet" href="../../css/css_descripcion/caracterisitcas.css">
    <link rel="stylesheet" href="../../css/css_descripcion/comentario.css">
    <link rel="stylesheet" href="../../css/css_descripcion/colores.css">
    <link rel="stylesheet" href="../../css/css_descripcion/forma_pago.css">
    <link rel="stylesheet" href="../../css/css_descripcion/descripcion_compra.css">
    <link rel="stylesheet" href="../../css/css_descripcion/modal.css">

    <link rel="stylesheet" href="../../css/reset.css">
    <link rel="stylesheet" href="../../css/footer.css">
    <link rel="stylesheet" href="../../css/estilos.css">
    <link rel="stylesheet" href="../../css/navbar.css">


    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/all.min.js"></script>
    <script src="../../js/main.js"></script>

@endsection



@section('main')

<main>
        <section class="descripcion_producto">
            <h1 id="nombrePrenda" key={{$prenda[0]->id}}>
                {{$prenda[0]->nombre}}
            </h1>
            <div class="contenedor_general">
                <div class="contenedor_galleria">
                    <div class="img_principal" id="big"  img='{{ $principal[0]->urlimg}}'>
                        <img src="../../storage/{{ $principal[0]->urlimg}}">
                    </div>
                    <div class="img_secundario" id="sub">
                        @for ($i = 0; $i < count($principal) ; $i++)
                          <img src="../../storage/{{$principal[$i]->urlimg}}">
                        @endfor
                       {{--  {{count($bidimensional[0])}}
                        <img src="../img/Ropa/momo02.jpg">
                        <img src="../img/Ropa/momo03.jpg">
                        <img src="../img/Ropa/momo04.jpg"> --}}
                    </div>
                </div>
                <div class="descripcion_ropa">
                    <div class="contenerdor_descripcion">
                        <div class="boton_desplediable">
                            <details>
                                <summary>Caracteristicas</summary>
                                <table>
                                    <tr>
                                        <td>Color: </td>
                                        <td id="colorPrendaDetails">{{ $principal[0]->color}}</td>
                                    </tr>
                                    <tr>
                                        <td>Composición: </td>
                                        <td>95% algodó</td>
                                    </tr>
                                    <tr>
                                        <td>Estilo:</td>
                                        <td>Casual</td>
                                    </tr>
                                    <tr>
                                        <td>Largo de manga:</td>
                                        <td>Manga larga</td>
                                    </tr>
                                    <tr>
                                        <td>Escote:</td>
                                        <td>Collar del soporte</td>
                                    </tr>
                                    <tr>
                                        <td>Tipo de patrón:</td>
                                        <td>A rayas</td>
                                    </tr>
                                    <tr>
                                        <td>Decoración:</td>
                                        <td>Tejido De Costilla</td>
                                    </tr>
                                    <tr>
                                        <td>Tela:</td>
                                        <td>La tela tiene algo</td>
                                    </tr>
                                    <tr>
                                        <td>Temporada:</td>
                                        <td>Otoño</td>
                                    </tr>
                                    <tr>
                                        <td>Tipo de ajuste:</td>
                                        <td>Ajustado</td>
                                    </tr>
                                    <tr>
                                        <td>Longitud:</td>
                                        <td>Regular</td>
                                    </tr>
                                    <tr>
                                        <td>Tipo de tapeta:</td>
                                        <td>Jerseys</td>
                                    </tr>
                                    <tr>
                                        <td>Ocasión: </td>
                                        <td>Fin de semana casual</td>
                                    </tr>
                                </table>
                            </details>
                        </div>
                        <div class="button_no_desplegable">
                            <h2>Caracteristicas</h2>
                            <table>
                                <tr>
                                    <td>Color: </td>
                                    <td>{{ $principal[0]->color}} </td>
                                </tr>
                                <tr>
                                    <td>Composición: </td>
                                    <td>95% algodó</td>
                                </tr>
                                <tr>
                                    <td>Estilo:</td>
                                    <td>Casual</td>
                                </tr>
                                <tr>
                                    <td>Largo de manga:</td>
                                    <td>Manga larga</td>
                                </tr>
                                <tr>
                                    <td>Escote:</td>
                                    <td>Collar del soporte</td>
                                </tr>
                                <tr>
                                    <td>Tipo de patrón:</td>
                                    <td>A rayas</td>
                                </tr>
                                <tr>
                                    <td>Decoración:</td>
                                    <td>Tejido De Costilla</td>
                                </tr>
                                <tr>
                                    <td>Tela:</td>
                                    <td>La tela tiene algo</td>
                                </tr>
                                <tr>
                                    <td>Temporada:</td>
                                    <td>Otoño</td>
                                </tr>
                                <tr>
                                    <td>Tipo de ajuste:</td>
                                    <td>Ajustado</td>
                                </tr>
                                <tr>
                                    <td>Longitud:</td>
                                    <td>Regular</td>
                                </tr>
                                <tr>
                                    <td>Tipo de tapeta:</td>
                                    <td>Jerseys</td>
                                </tr>
                                <tr>
                                    <td>Ocasión: </td>
                                    <td>Fin de semana casual</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="contenedor_compra">
                    <div class="descripcion_compra">
                        <div class="Preciso_ropa">
                            <P><del> S./799.99</del></P>
                            <p id="precioPrenda" precio={{$prenda[0]->precio}}>S./{{$prenda[0]->precio}}</p>
                        </div>
                        <div class="Talla">
                            <h2>Talla</h2>
                            <br>
                            <div class="tallas_tama">

                                @foreach($listaStock as $listar )
                                <label class="talla_container">
                                    <input type="radio" name="radioTallas" keyTalla={{$listar->talla->id}} value="{{$listar->talla->talla}}" stock={{$listar->stock}} class="talla_radio">
                                    <span stock={{$listar->stock}} class="tallas_checkmark">{{$listar->talla->talla}}</span>
                                </label>
                                {{-- <label class="talla_container">
                                    <input type="radio"  name="radio" class="talla_radio">
                                    <span class="tallas_checkmark">M</span>
                                </label>
                                <label class="talla_container">
                                    <input type="radio" name="radio" class="talla_radio">
                                    <span class="tallas_checkmark">L</span>
                                </label>
                                <label class="talla_container">
                                    <input type="radio"name="radio" class="talla_radio">
                                    <span class="tallas_checkmark">XL</span>
                                </label> --}}

                                @endforeach

                            </div>
                        </div>
                        <div class="tock_cantidad">
                            <div class="tock">
                                <h2>Stock</h2>
                                <h3 id="stockPrendaTalla">-</h3>
                            </div>
                            <div class="cantidad">
                                <h2>Cantidad</h2>

                                <input id="cantidadPrenda" type="number" value="1">
                            </div>
                        </div>
                    </div>
                    <div class="boton_compra">
                        <button id="modal_open" class="button_o">
                            Añadir al Carrito
                        </button>
                    </div>

                    <section id="openModal" class="modal_option">
                        <div class="modal">
                            <a href="#" class="close">X</a>
                            <h2>Se Agrego al Carrito</h2>
                            <br>
                            <a href="{{route('compras')}}" class="modal_btn1"> ir al carrito de compras
                                <i class="fas fa-arrow-right"></i></a>
                            <br>
                            <br>
                            <a href="{{ route('prendas')}}" class="modal_btn2">
                                <i class="fas fa-arrow-left"></i> seguir comprando</a>
                        </div>
                    </section>
                    <section id="openModal2" class="modal_option2">
                        <div class="modal">
                            <a href="#" class="close">X</a>
                            <h2>Error</h2>
                            <br>
                            <a href="shopping_card.html" class="modal_btn1"> Ingresa cantidad a comprar
                               </a>
                           
                        </div>
                    </section>
                    <section id="openModal3" class="modal_option3">
                        <div class="modal">
                            <a href="#" class="close">X</a>
                            <h2>Error</h2>
                            <br>
                            <a href="shopping_card.html" class="modal_btn1">Elige tu talla
                               </a>
                           
                        </div>
                    </section>
                </div>
            </div>

        </section>
        <section class="other_color">
            <h1>Otros Colores:</h1>
            <div class="contenedor_colores">

                @for ($i = 0; $i < count($bidimensional) ; $i++)
                        
                            <a class="imagenColor" href="http://creaciones.test/prendaDescripcion/{{$bidimensional[$i][0]->prenda_id}}/{{$bidimensional[$i][0]->color}}">
                                <img src="../../storage/{{$bidimensional[$i][0]->urlimg}}" alt="color">
                            </a>
                       
              @endfor
               {{--  <div class="repositorio_color">
                    <a href="#">
                        <img src="../img/Ropa/verde.jpg" alt="color">
                    </a>
                </div>
                <div class="repositorio_color">
                    <a href="#">
                        <img src="../img/Ropa/amarillo.jpg" alt="color">
                    </a>
                </div>
                <div class="repositorio_color">
                    <a href="#">
                        <img src="../img/Ropa/modado.jpg" alt="color">
                    </a> --}}
                </div>

                <div id="cantImagenes" style="display: none">{{count($bidimensional)}}</div>

                <div id="descripcionArrowRight"><img width="40px" src="../../iconos/arrowrigth.svg"></div>

                <div id="descripcionArrowLeft"><img width="40px" src="../../iconos/arrowleft.svg"></div>
            
        </section>
        <section class="forma_pago">
            <h1>Formato de Pago</h1>
            <div class="contenedor_pago">
                <div class="repositorio_pago">
                    <img src="../../img/tipo_compra/credit-card22.png" alt="">
                    <br>
                    Tarjeta visa
                </div>
                <div class="repositorio_pago">
                    <img src="../../img/tipo_compra/withdraw2.png" alt="">
                    <br>
                    Transferencia bancaria
                </div>
                <div class="repositorio_pago">
                    <img src="../../img/tipo_compra/buy copia.png" alt="">
                    <br>
                    Contra entrega
                </div>
            </div>
        </section>
        <section class="comentarios">
            <div class="contenerdor_comentario">
                <details>
                    <summary>Comentarios</summary>

                    @forelse($comentarios as $listar)
                            <div class="comntarios_usu">
                                
                                    <h2>{{$listar->user->name}}</h2>
                                    <h3>{{$listar->created_at}}</h3>
                             
                                
                                    <p>
                                        {{$listar->comentario}}
                                    </p>
                             

                            </div>

                    @empty

                     <div class="comntarios_usu">
                            <p>
                                no hay comentarios
                            </p>
                        </div>

                    @endforelse

                    {{-- <div class="comntarios_usu">
                        <h2>Alexandra P.</h2>
                        <h3>25 diciembre 2022</h3>
                        <p>
                            Vestido Perfecto que se adapta perfectamente a tus curvas. ¡He prestado este vestido a
                            varios amigos y a todos les encanta! Gran material y perfecto para múltiples ocasiones.
                            Recomiendo altamente 10/10
                        </p>
                    </div>
                    <div class="comntarios_usu">
                        <h2>Alexandra P.</h2>
                        <h3>25 diciembre 2022</h3>
                        <p>
                            Vestido Perfecto que se adapta perfectamente a tus curvas. ¡He prestado este vestido a
                            varios amigos y a todos les encanta! Gran material y perfecto para múltiples ocasiones.
                            Recomiendo altamente 10/10
                        </p>
                    </div> --}}
                </details>
            </div>
            <div class="caja_comentario">
                <h1>Comentarios</h1>
                <div style="display: none" id="npaginas">{{$total}}</div>
                <div id="allComents">
                @forelse($comentarios as $listar)
                <div class="comntarios_usu">
                    <div class="usuario">
                        <h2>{{$listar->user->name}}</h2>
                        <h3>{{$listar->created_at}}</h3>
                    </div>
                    <div class="usu_comentario">
                        <p>
                            {{$listar->comentario}}
                        </p>
                    </div>

                </div>

                @empty

                 <div class="usu_comentario">
                        <p>
                            no hay comentarios
                        </p>
                    </div>

                @endforelse
                </div> 
              
                <button id="comentariosCargar">Cargar mas Comentarios</button>
            </div>
        </section>
    </main>

@endsection


@section('fieldjs')


<script src="../../js/descripcion_prenda/model.js"></script>
    <script src="../../js/ropa.js"></script>


@endsection