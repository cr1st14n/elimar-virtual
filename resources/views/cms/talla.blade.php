@extends("cms.layout")


@section('content')



<div class="registrar caja">
	<h1 class="titulos">Registrar Tallas</h1>

	<form method="post" action="{{route('talla.store')}}">
		@csrf

		<input type="text" placeholder="talla" name="talla"><br>

		<input type="text" placeholder="descripcion" name="descripcion"><br>

		<input type="submit" name="">

	</form>
	

</div>






<div class="tabla-pos2 caja-lg">
	<h1 class="titulos">Tallas</h1>

	<table class="tabla-lista">
		<thead>
			<tr><th>Id</th> <th>Talla</th> <th>Descripcion</th></tr>
		</thead>
		<tbody>

			@forelse($talla as $tallas)

			  @if($tallas->state)

			<tr><td>{{$tallas->id}}</td><td>{{$tallas->talla}}</td><td>{{$tallas->descripcion}}</td><td class="actualizar"><button key="{{$tallas->id}}" catalogo="talla"  class="editar-modal">Editar</button></td><td  class="eliminar"><form method="POST" action="{{route('talla.destroy',$tallas->id)}}">
				@csrf
				{!! method_field('DELETE') !!}
				<button type="submit">eliminar</button></form></td></tr>

			  @endif
			@empty

			<tr><td>vacio</td><td>vacio</td><td>vacio</td><td class="actualizar"><button class="editar-modal">Editar</button></td><td class="eliminar"><button>eliminar</button></td></tr>

			@endforelse
			
		</tbody>
	</table>
	

</div>


@include('cms.partials.modalTalla')
@endsection

@section('enlacesjs')

<script type="text/javascript" src="js/cms/talla.js"> </script>

@endsection