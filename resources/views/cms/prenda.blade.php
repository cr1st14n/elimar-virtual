@extends("cms.layout")



@section('content')



<div> <button id="btn-agregar" class="btn-agregar">agregar</button></div>




<div class="tabla-pos1 caja-lg">
	<h1 class="titulos">Prendas</h1>

	<table class="tabla-lista">
		<thead>
			<tr><th>Id</th> <th>Tipo</th> <th>Nombre</th><th>Descripcion</th><th>Costo</th></tr>
		</thead>
		<tbody>

			@forelse($prendas as $listar)

			
			<tr><td>{{$listar->id}}</td><td>{{$listar->tipo->tipo}}</td><td>{{$listar->nombre}}</td><td>{{$listar->descripcion}}</td><td>{{$listar->precio}}</td><td key="{{$listar->id}}"  id="btn-stock" class="actualizar btn-stock"><button>stock</button></td><td class="actualizar"><button key="{{$listar->id}}" tipo="{{$listar->tipo->tipo}}" id="btn-imagenes" class="btn-imagenes" >imagenes</button></td><td><button key="{{$listar->id}}" class="btn-Editar" id="btn-Editar">editar</button></td></tr>


			@empty

			 <tr><td>vacio</td><td>vacio</td><td>vacio</td><td>vacio</td><td>vacio</td><td id="btn-stock" class="actualizar"><button>stock</button></td><td class="actualizar"><button id="btn-imagenes" class="btn-imagenes" >imagenes</button></td><td><button id="btn-Editar">editar</button></td></tr>

			 @endforelse


		</tbody>
	</table>
	

</div>

@include('cms.partials.modalPrendaStock')

@include('cms.partials.modalPrendaAgregar')

@include('cms.partials.modalPrendaImagenes')

@include('cms.partials.modalPrendaEditar')

@endsection

@section('enlacesjs')




<script type="text/javascript" src="js/cms/prendaStock.js"> </script>

<script type="text/javascript" src="js/cms/prendaTipo.js"> </script>

<script type="text/javascript" src="js/cms/prenda.js"> </script>


@endsection
