@extends('cms.layout')



@section('content')



<div class="registrar caja">
	<h1 class="titulos">Registrar Tipo Prenda</h1>

	<form method="POST" action="{{route('tipoPrenda.store')}}">

		@csrf
		<input type="text" placeholder="tipo prenda" name="tipo"><br>

		<textarea name="descripcion" placeholder="descripcion"></textarea>

		<input type="submit" name="">

	</form>
	

</div>






<div class="tabla-pos2 caja-lg">
	<h1 class="titulos">Tipo Prenda</h1>

	<table class="tabla-lista">
		<thead>
			<tr><th>Id</th> <th>Tipo Prenda</th> <th>Descripcion</th></tr>
		</thead>
		<tbody>
			@forelse($tipoPrenda as $listar)

			  @if($listar->state)

			<tr><td>{{$listar->id}}</td><td>{{$listar->tipo}}</td><td>{{$listar->descripcion}}</td><td class="actualizar"><button key="{{$listar->id}}" catalogo="talla"  class="editar-modal">Editar</button></td><td  class="eliminar"><form method="POST" action="{{route('tipoPrenda.destroy',$listar->id)}}">
				@csrf
				{!! method_field('DELETE') !!}
				<button type="submit">eliminar</button></form></td></tr>

			  @endif
			@empty

			<tr><td>vacio</td><td>vacio</td><td>vacio</td><td class="actualizar"><button class="editar-modal">Editar</button></td><td class="eliminar"><button>eliminar</button></td></tr>

			@endforelse
		</tbody>
	</table>
	

</div>


@include('cms.partials.modalTipoPrenda')

@endsection


@section('enlacesjs')

<script type="text/javascript" src="js/cms/tipoPrenda.js"> </script>

@endsection