<!DOCTYPE html>
<html>
<head>
	<title>CMS</title>
	<link rel="stylesheet" type="text/css" href="css/cms/reset.css">
	<link rel="stylesheet" type="text/css" href="css/cms/header.css">
	
	<link rel="stylesheet" type="text/css" href="css/cms/estilosElimar.css">
	<link rel="stylesheet" type="text/css" href="css/cms/aside.css">
	<link rel="stylesheet" type="text/css" href="css/cms/contenido.css">
	<link rel="stylesheet" type="text/css" href="css/cms/talla.css">
	<link rel="stylesheet" type="text/css" href="css/cms/mdlPrendaStock.css">
	<link rel="stylesheet" type="text/css" href="css/cms/mdlPrendaAgregar.css">
	<link rel="stylesheet" type="text/css" href="css/cms/mdlPrendaImagenes.css">
	<link rel="stylesheet" type="text/css" href="css/cms/mdlPrendaEditar.css">
	<link rel="stylesheet" type="text/css" href="css/cms/ventas.css">
</head>
<body>


{{-- contenido GESTOR --}}

<aside>


	<div>
		<img src="iconos/elimaricon.jpg" >
	</div>

	<div>

		<div id="opciones">
			<h1>Gestion</h1>

				<ul>
					<a href="{{route('ventas.index')}}"><li>Venta<img  src="iconos/circle-outline.png"></li> </a>
					<a href="{{route('prenda.index')}}"><li>Prenda<img  src="iconos/circle-outline.png"></li></a>
					<a href=""><li>Usuarios<img  src="iconos/circle-outline.png"></li></a>
					<a href=""><li>Comentarios<img  src="iconos/circle-outline.png"></li></a>
				</ul>

			
		</div>

				
        <div id="opciones">
			<h1>Configuracion</h1>

				<ul>
					<a href="{{route('tipoPrenda.index')}}"><li>Tipo Prenda<img  src="iconos/circle-outline.png"></li></li></a>
					<a href="{{route('talla.index')}}"><li>Tallas<img  src="iconos/circle-outline.png"></li></a>
					<a href="{{route('formaPago.index')}}"><li>Forma Pago<img  src="iconos/circle-outline.png"></li></a>
					<a href="{{route('estadoVenta.index')}}"><li>Estados Venta<img  src="iconos/circle-outline.png"></li></a>
				</ul>

			
		</div>
				 
		
	</div>




	
</aside>

<section id="contenido">

	<header>
	
   </header>


  <section id="main">

  	@yield('content')
	
  </section>
	

	
</section>



   @yield('enlacesjs')

{{-- FIN GESTOR --}}




</body>
</html>