@extends('cms.layout')


@section('content')

<div class="registrar caja">
	<h1 class="titulos">Registrar Forma de pago</h1>

	<form method="POST" action="{{route('formaPago.store')}}">
		@csrf
		<input type="text" placeholder="forma de pago" name="fpago"><br>

		<textarea name="descripcion" placeholder="Descripcion"></textarea> 

		<input type="submit" name="">

	</form>
	

</div>


<div class="tabla-pos2 caja-lg">
	<h1 class="titulos">Formas de pago</h1>

	<table class="tabla-lista">
		<thead>
			<tr><th>Id</th> <th>Forma Pago</th> <th>Descripcion</th></tr>
		</thead>
		<tbody>
		@forelse($formaPago as $listar)

			  @if($listar->state)

			<tr><td>{{$listar->id}}</td><td>{{$listar->fpago}}</td><td>{{$listar->descripcion}}</td><td class="actualizar"><button key="{{$listar->id}}" catalogo="talla"  class="editar-modal">Editar</button></td><td  class="eliminar"><form method="POST" action="{{route('formaPago.destroy',$listar->id)}}">
				@csrf
				{!! method_field('DELETE') !!}
				<button type="submit">eliminar</button></form></td></tr>

			  @endif
			@empty

			<tr><td>vacio</td><td>vacio</td><td>vacio</td><td class="actualizar"><button class="editar-modal">Editar</button></td><td class="eliminar"><button>eliminar</button></td></tr>

			@endforelse
		</tbody>
	</table>
	

</div>

@include('cms.partials.modalFormaPago')

@endsection

@section('enlacesjs')

<script type="text/javascript" src="js/cms/formaPago.js"> </script>

@endsection
