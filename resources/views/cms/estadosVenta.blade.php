@extends('cms.layout')


@section('content')


<div class="registrar caja">
	<h1 class="titulos">Registrar Estados Venta</h1>
	

	<form method="POST" action="{{route('estadoVenta.store')}}">
		@csrf
		<input type="text" placeholder="Estado" name="estado"><br>

		<textarea name="descripcion" placeholder="Descripcion"></textarea> 

		<input type="submit" name="">

	</form>
	

</div>






<div class="tabla-pos2 caja-lg">
	<h1 class="titulos">Estados Venta</h1>

	<table class="tabla-lista">
		<thead>
			<tr><th>Id</th> <th>Estados Venta</th> <th>Descripcion</th></tr>
		</thead>
		<tbody>
			@forelse($estadoVenta as $listar)

			  @if($listar->state)

			<tr><td>{{$listar->id}}</td><td>{{$listar->estado}}</td><td>{{$listar->descripcion}}</td><td class="actualizar"><button key="{{$listar->id}}" catalogo="talla"  class="editar-modal">Editar</button></td><td  class="eliminar"><form method="POST" action="{{route('estadoVenta.destroy',$listar->id)}}">
				@csrf
				{!! method_field('DELETE') !!}
				<button type="submit">eliminar</button></form></td></tr>

			  @endif
			@empty

			<tr><td>vacio</td><td>vacio</td><td>vacio</td><td class="actualizar"><button class="editar-modal">Editar</button></td><td class="eliminar"><button>eliminar</button></td></tr>

			@endforelse
		</tbody>
	</table>
	

</div>
@include('cms.partials.modalEstadoVenta')

@endsection


@section('enlacesjs')

<script type="text/javascript" src="js/cms/estadoVenta.js"> </script>

@endsection