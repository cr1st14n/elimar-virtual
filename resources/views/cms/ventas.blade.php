@extends('cms.layout')



@section('content')


                      <div class="buscadorVentas"><label>Buscar por</label><select id="buscadorVentas">
	                         <option>--selecione--</option>
	                         <option value="desc">nuevos</option>
	                         <option value="asc">antiguo</option>
	                           
                            </select>
                        </div>


                            <div  class="selectDate">
                            	<form method="get" action="http://creaciones.test/ventas">
                            		
                            	
                            	<label>DESDE: </label><input type="date" name="from" value="">

                            	<label>HASTA: </label><input type="date" name="to" value="">


                            	<button type="submit">Buscar</button>

                            	</form>
                            </div>


<div>
	<h1 class="titulos">Ventas</h1>

	<table class="tabla-ventas">
		<thead>
			<tr><th>Id</th> <th>Cliente</th> <th>Fecha</th><th>Importe</th><th>Costo Envio</th><th>Total</th><th>Forma Pago</th><th>Estado</th><th></th><th></th><th></th></tr>
		</thead>
		<tbody>


			@forelse($contenido as $listar)

			
            
			 <tr><td>{{$listar->id}}</td><td>{{$listar->user->name}}</td><td>{{$listar->created_at}}</td><td>{{$listar->total}}</td><td>{{$listar->cenv}}</td><td>{{$listar->totalp}}</td><td>{{$listar->forma_pago->fpago}}</td><td > 
			 	@csrf
			 	<select key="{{$listar->id}}" class="estadoPedido">

			 		@forelse($estados as $listado)

			 		@if($listado->id == $listar->estado->id)

			 		  <option selected="selected" value="{{$listado->id }}">{{$listado->estado}}</option>

			 		 @else

			 		 <option value="{{$listado->id }}">{{$listado->estado}}</option>

			 		 @endif



			 		
			 		@empty
			 		<option>vacio</option>

			 		@endforelse

			 	</select></td>

			 	<td><button  key={{$listar->id}} class="venta-direccion">direccion</button></td><td><button key={{$listar->id}} user={{$listar->user->id}} class="venta-ver">ver</button></td><td><form method="POST" action="http://creaciones.test/ventas/{{$listar->id}}">
			 		 @csrf
			 		 {!! method_field('DELETE') !!}

			 		<button type="submit" class="venta-eliminar">eliminar</button>
			 		
			 	</form>
			 		</td></tr>

			{{--  <tr><td>vacio</td><td>cristian alvarez</td><td>vacio</td><td>vacio</td><td>vacio</td><td>vacio</td><td>vacio</td><td ><select></select></td><td><button>direccion</button></td><td><button>ver</button></td><td><button>eliminar</button></td></tr>
 --}}         @empty


              <tr><td>vacio</td><td>cristian alvarez</td><td>vacio</td><td>vacio</td><td>vacio</td><td>vacio</td><td>vacio</td><td ><select></select></td><td><button>direccion</button></td><td><button>ver</button></td><td><button>eliminar</button></td></tr>


             @endforelse

		</tbody>
	</table>

	<div>
		{{$contenido->links()}}
	</div>
	

</div>



<div id="cajaVentas" class="cajaVentas">
	<div  id="ventaDireccionModal" class="modal-into">

		<div>
			<h2></h2>
			<label></label>
		</div>


		<div>
			<h2></h2>
			<label></label>
		</div>

		<div>
			<h2></h2>
			<label></label>
		</div>


		<div>
			<h2></h2>
			<label></label>
		</div>


		<div>
			<h2></h2>
			<label></label>
		</div>


		<div>
			<h2></h2>
			<label></label>
		</div>
		
	</div>
</div>


<div id="modalDetalleVenta" class="modalDetalleVenta">



	<div  id="DetalleVentaContenido" class="contenidoModal">


		<div class="pulpa">

			<div>
				<img src="../img/Ropa/large.jpg" width="180px">
			</div>

			<div>

				<div>

				<label>
				<h2>Diseño:&nbsp;</h2>

				pantalo rayas

				</label>

				<label>
					<h2>Talla:&nbsp;</h2>
					S
					
				</label>

				</div>

				<div>
				<label>
					<h2>Cantidad:&nbsp;</h2>
					1
					
				</label>

				<label>
					<h2>Color:&nbsp;</h2>
                       ROSADO
				</label>
				</div>

				<div>
					
				<label>
					<h2>Precio:&nbsp;</h2>

					S/.20.00
					
				</label>
				<label>
					<h2>Total:&nbsp;</h2>

					S/.20.00
					
				</label>

				</div>
			</div>

			
			

		</div>


		<div class="pulpa">

			<div>
				<img src="../img/Ropa/large.jpg" width="180px">
			</div>

			<div>

				<div>

				<label>
				<h2>Diseño:&nbsp;</h2>

				pantalo rayas

				</label>

				<label>
					<h2>Talla:&nbsp;</h2>
					S
					
				</label>

				</div>

				<div>
				<label>
					<h2>Cantidad:&nbsp;</h2>
					1
					
				</label>

				<label>
					<h2>Color:&nbsp;</h2>
                       ROSADO
				</label>
				</div>

				<div>
					
				

				<label>
					<h2>Precio:&nbsp;</h2>

					S/.20.00
					
				</label>
				<label>
					<h2>Total:&nbsp;</h2>

					S/.20.00
					
				</label>

				</div>
				
			</div>

			
			

		</div>
		
		



	</div>
	


</div>





@endsection


@section('enlacesjs')

<script type="text/javascript" src="js/cms/ventas.js"> </script>

@endsection