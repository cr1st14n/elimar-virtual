<div  id="modal-editar" class="registrar modal caja">
	<h1 class="titulos">Editar</h1>

	<form method="POST" id="formulario-modal" action="">

		{!! method_field('PUT') !!}
		@csrf

		
		<input id="modal-name" name="estado" type="text" placeholder="Estado Venta" >

		<textarea id="modal-descripcion" name="descripcion" placeholder="Descripcion"></textarea> 

		<button type="submit">Actualizar</button>

		<button id="modal-cancelar">cancelar</button>

	</form>
	

</div>