@extends('layout')


@section('headers')


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contra Entrega</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar.css">

    <link rel="stylesheet" href="css/pago/contra_entrega.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/main.js"></script>

@endsection



@section('main')


 <main>
        <section>
                    <h1 class="titulo">Contra Entrega</h1>
                    <div class="contendor_general">
                            <div class="contenedor">
                                    <form>
                                        @csrf
                                        <div>
                                                <label>Nombre del Titular</label>
                                                <input type="text" id="nombreTitular">
                                                <a href="#" id="alertName">asdsa</a>
                                        </div>
                                        <div>
                                                <label>Apellido</label>
                                                <input type="text" id="apellidoTitular">
                                                <a href="#" id="alerLasttName"></a>
                                        </div>
                                        <div>
                                            <label>Distrito</label>
                                            <input type="text" id="distrito">
                                             <a href="#" id="alertDistrito"></a>
                                        </div>
                                        <div>
                                                <label>Dirección</label>
                                                <input type="text" id="direccion">
                                                <a href="#" id="alertDireccion"></a>
                                        </div>
                                        <div>
                                                <label>Referencia</label>
                                                <input type="text" id="referencia">
                                                <a href="#" id="alertReferencia"></a>
                                        </div>
                                        <div class="contenedor_telefono">
                                                <div>
                                                        <label>Telefono</label>
                                                        <input type="text" id="telefono1">

                                                        <a href="#" id="alertTelefono1"></a>
                                                </div>
                                                <div>
                                                        <label>celular.</label>
                                                        <input type="text" id="telefono2">
                                                        <a href="#" id="alertTelefono2"></a>
                                                </div>
                                        </div>
                                        <button id="procesarVenta">Realizar Compra</button>
                                    </form>
                                    
                                </div>
                                <div class="contenedor_img">
                                    <h2>Nos comunicaremos con usted.</h2>
                                    <div>
                                            <div class="img">
                                                    <img src="img/cell.png" alt="png" id="celular">
                                                </div>
                                                <div class="person">
                                                    <img src="img/men.png" alt="img" id="humano">
                                                </div>
                                    </div>
                                </div>
                    </div>


                     <section id="openModal" class="modal_option">
                        <div class="modal">
                            <a href="#" class="close">X</a>
                            <h2>MENSAJE:</h2>
                            <br>
                            <a href="shopping_card.html" class="modal_btn1"> ir al carrito de compras
                                </a>
                           
                    </section>
                    
        </section>
    </main>


@endsection



@section('fieldjs')

<script src="js/animacion.js"></script>

<script src="js/procesarVenta.js"></script>



@endsection