@extends('layout')


@section('headers')

 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/navbar.css">


    <link rel="stylesheet" href="css/comentario_prenda/perfil_usuario.css">
    <link rel="stylesheet" href="css/comentario_prenda/comentario_prenda.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/main.js"></script>



@endsection



@section('main')

<main>
        <div class="usuario_comentario">
            <div class="perfil_usuario">
                <h1>Perfil de usuario</h1>
                <div>
                    <h2>Usuario:</h2>
                    <h3>{{Auth::user()->name}}</h3>
                </div>
                <div>
                    <h2>Correo:</h2>
                    <h3>{{Auth::user()->email}}</h3>
                </div>
                <div class="contenedor_bton">
                    <div>
                        <a href="{{route('compras')}}">Comprar</a>
                    </div>
                    <div>
                        <a href="{{route('resenas.index')}}">Reseña</a>
                    </div>
                </div>
            </div>
            <div class="contenedor_comentario">
                <h1>Comentarios</h1>
                <div class="caja_contenedor">

                @forelse($result as $listar)

                    <div class="comentarios_usuarios">
                        <div class="caja_comentario">
                            <h2>{{$listar->created_at}}</h2>
                            <p>{{$listar->comentario }}</p>
                        </div>
                        <div class="caja_img">
                            <img src="storage/{{$listar->prenda->image[0]->urlimg }}" alt="Ropa">
                            
                            <div class="botton_flotante">
                                <a href="#">Ver</a>
                            </div>
                        </div>
                    </div>

                @empty

                <div class="comentarios_usuarios">
                        No hay comentarios
                    </div>

                @endforelse


                <div>{{$result->links() }}</div>
                    {{-- <div class="comentarios_usuarios">
                        <div class="caja_comentario">
                            <h2>15/06/12</h2>
                            <p>Realmente una prenda de calidad los bordes me gust
                                aron mucho FELICITACIONES !!!</p>
                        </div>
                        <div class="caja_img">
                            <img src="img/Ropa/Park_So_Dam.jpg" alt="Ropa">
                            
                            <div class="botton_flotante">
                                <a href="#">Ver</a>
                            </div>
                        </div>
                    </div>
                    <div class="comentarios_usuarios">
                        <div class="caja_comentario">
                            <h2>15/06/12</h2>
                            <p>Realmente una prenda de calidad los bordes me gust
                                aron mucho FELICITACIONES !!!</p>
                        </div>
                        <div class="caja_img">
                            <img src="img/Ropa/Park_So_Dam.jpg" alt="Ropa">
                            
                            <div class="botton_flotante">
                                <a href="#">Ver</a>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </main>


@endsection