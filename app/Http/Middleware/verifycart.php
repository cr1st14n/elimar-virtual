<?php

namespace App\Http\Middleware;

use Closure;

use App\color;

class verifycart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $array=json_decode($request->prendas,true);


          $nombre=$request->nombre;
          $apellido=$request->apellido;
          $distrito=$request->distrito;
          $direccion=$request->direccion;
          $referencia=$request->referencia;
          $telf=$request->telefono1;
          $telfopc=$request->telefono2;



          $cadena=strval($telfopc);
          $longitud=strlen($cadena);



          if(is_null($nombre) || is_null($apellido) || is_null($distrito) || is_null($direccion) || is_null($referencia) || is_null($telf) || is_null($telfopc)){
            
             // $response=collect(["ok"=>false,"msj"=>"falta llenar uno de los campos del formulario"]);

            return response()->json([
                'ok' => false,
                'msj' => "falta llenar uno de los campos del formulario"
            ], 500); 
           

          }else if($longitud<9 ||  $longitud!=9){

            // $response=collect(["ok"=>false,"msj"=>"numero de celular debe tener 9 digitos"]);

            return response()->json([
                'ok' => false,
                'msj' => "numero de celular debe tener 9 digitos",
                'nro'=>$longitud
            ], 500);  

          }



         for ($i=0; $i <count($array) ; $i++) { 

          $idprenda =$array[$i]['id'];
          $talla=$array[$i]['keytalla'];
          $color=$array[$i]['color'];
          $cantidad=$array[$i]['cantidad'];


          if(is_null($talla) || $talla==" "){

            // $response=collect(["ok"=>false,"msj"=>"no tiene talla una de las prendas"]);

            return response()->json([
                'ok' => false,
                'msj' => "no tiene talla una de las prendas"
            ], 500); 

          }

          if(is_null($color) || $color==" "){

            // $response=collect(["ok"=>false,"msj"=>"no tiene color una de las prendas"]);

            return response()->json([
                'ok' => false,
                'msj' => "no tiene color una de las prendas"
            ], 500); 

          }


          $color=color::where('prenda_id',$idprenda)
                 ->where('color',$color)
                 ->where('talla_id',$talla)->first();

          $stock=$color->stock;

          if(is_null($cantidad) || $cantidad<=0 || $cantidad>$stock){

            // $response=collect(["ok"=>false,"msj"=>"la cantidad no es valida"]);

            return response()->json([
                'ok' => false,
                'msj' => "la cantidad no es valida",
                'stock'=>$stock
            ], 500); 

          }


      }
        return $next($request);
    }
}
