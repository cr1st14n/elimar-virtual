<?php

namespace App\Http\Controllers;

use App\tipo;

use Illuminate\Http\Request;

class tipoPrendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    
     */
     public function __construct(){

      $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth',['except'=>['show']]);


 }

    public function index()
    {
        
          $tipoPrenda = tipo::all();

        return view('cms.tipoPrenda',compact('tipoPrenda'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $tipo=$request->input('tipo');

        $tipoPrenda= tipo::where('tipo',$tipo)->first();


        if($tipoPrenda){

            $tipoPrenda->state=true;

               $tipoPrenda->save();

        }else{


            tipo::create($request->all());

    

        }

           return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
             $resultado=tipo::find($id);      
    

        return $resultado;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateTipo=tipo::find($id);

          $updateTipo->update($request->all());


          return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $tipo= tipo::find($id);

      $tipo->state=false;

      $tipo->save();

      return back();
    
    }
}
