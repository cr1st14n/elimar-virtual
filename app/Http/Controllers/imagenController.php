<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\image;

use Illuminate\Support\Facades\Storage;

class imagenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('urlimg')){
             $file= $request->file('urlimg')->store('public');
             $name=explode("/",$file);
             $name=$name[1];
             // $file->move(public_path()."/images"."/".$request->input('tipo')."/".$name);
        }

        // $url="http://creaciones.test/images"."/".$request->input('tipo')."/".$name;


        $prendaImg= new image();
        $prendaImg->prenda_id=$request->input('prenda_id');
        $prendaImg->color=$request->input('color');
        $prendaImg->urlimg=$name;
        $prendaImg->save();
        

        $id=$request->input('prenda_id');

        $result=image::where('prenda_id',$id)->orderBy('color')->get();


        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

         $result=image::where('prenda_id',$id)->orderBy('color')->get();


         return $result;



     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        
        $idprenda=$request->input('prenda_id');
        $color=$request->input('color');
        $imagen=$request->input('imagen');

       image::where(['prenda_id'=>$idprenda,
                      'color'=>$color])->delete();


        Storage::delete('public/'.$imagen);


         $result=image::where('prenda_id',$idprenda)->orderBy('color')->get();


        return $result;
    }
}
