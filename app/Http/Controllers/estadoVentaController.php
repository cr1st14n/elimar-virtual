<?php

namespace App\Http\Controllers;


use App\estado;

use Illuminate\Http\Request;

class estadoVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct(){

      $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth',['except'=>['show']]);


 }
 
    public function index()
    {
        
        $estadoVenta= estado::all();

        return view('cms.estadosVenta',compact('estadoVenta'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $estado=$request->input('estado');

        $buscando= estado::where('estado',$estado)->first();


        if($buscando){

            $buscando->state=true;

               $buscando->save();

        }else{


            estado::create($request->all());

    

        }

           return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resultado=estado::find($id);      
    

        return $resultado;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $updateEstado=estado::find($id);

          $updateEstado->update($request->all());


          return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $estadoVenta= estado::find($id);

      $estadoVenta->state=false;

      $estadoVenta->save();

      return back();
    }
}
