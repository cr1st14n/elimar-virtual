<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\coment;

class getComentariosController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,$id)
    {
        //

        $comentarios=coment::where('prenda_id',$id)->with('user')->paginate(2);
        

         $total=$comentarios->lastPage();
       

        return view('comentariosPaginate',compact('comentarios','total'));
    }
}
