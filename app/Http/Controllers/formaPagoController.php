<?php

namespace App\Http\Controllers;
use App\forma_pago;

use Illuminate\Http\Request;

class formaPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

 public function __construct(){

      $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth',['except'=>['show']]);


 }


    public function index()
    {


        
        $formaPago= forma_pago::all();

        return view('cms.formaPago',compact('formaPago'));

   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $formaPago=$request->input('fpago');

        $buscando= forma_pago::where('fpago',$formaPago)->first();


        if($buscando){

            $buscando->state=true;

               $buscando->save();

        }else{


            forma_pago::create($request->all());

    

        }

           return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $resultado=forma_pago::find($id);      
    

        return $resultado;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $updateFormaPago=forma_pago::find($id);

          $updateFormaPago->update($request->all());


          return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fpago=forma_pago::find($id);

      $fpago->state=false;

      $fpago->save();

      return back();
    }
}
