<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\venta;

use App\detail;

use Illuminate\Support\Facades\Auth;


class comprasController extends Controller
{


    public function __construct(){


        // $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth');


    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $id=intval(Auth::id());

         $arrayCompras=venta::where('user_id',$id)->with('forma_pago')->with('estado')->paginate(3);


         // return $arrayCompras;
        


        return view('estadoPedido',compact('arrayCompras'));
    }



    public function detalles($id)
    {

      

          $detalles=detail::where('venta_id',$id)->with(['prenda','prenda.coment'=> function($query) use ($id){
              $query->where('user_id',Auth::id())->where('venta',$id);
          }])->get();

             
          return $detalles;


    }
}
