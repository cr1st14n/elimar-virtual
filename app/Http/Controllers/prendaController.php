<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\prenda;

class prendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct(){


        $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth',['except'=>['show']]);


    }


    public function index()
    {

        $prendas=prenda::with('tipo')->get();

        return view('cms.prenda',compact("prendas"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        


            prenda::create($request->all());

    

        

           return back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $resultado=prenda::find($id);      
    

        return $resultado;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $updateTalla=prenda::find($id);

          $updateTalla->update($request->all());


          return back();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
