<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\color;

class dataColorController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id,Request $request)
    {


        $consultas=color::where('prenda_id',$id)->get();
       

       $array=[];

        for ($i=0; $i < $consultas->count() ; $i++) { 
           
            $array[$i]=$consultas[$i]->only(['color']);
        }

         // $rpta = $consultas[0]->only(['color']);


        $collection = collect($array);

        $unique=$collection->unique();

        return $unique->values()->all();
        


      
    }
}
