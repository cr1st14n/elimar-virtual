<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\detail;

use App\venta;

use App\color;

use App\prenda;

use App\direccion;
use App\estado;

use App\talla;



use Illuminate\Support\Facades\Auth;

class ventasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){


        

        // $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth',['except'=>['show']]);

        $this->middleware('verifycart',['only'=>['store']]);


    }


    public function index(Request $request)
    {

       if(isset($request->orden)){

          $valor=strval($request->orden);

          if ($valor=="asc") {
            $contenido=venta::with('user')->with('estado')->with('forma_pago')->orderBy('created_at','asc')->paginate(6);
          }else if($valor=="desc"){
            $contenido=venta::with('user')->with('estado')->with('forma_pago')->orderBy('created_at','desc')->paginate(6);
          }




       }else if(isset($request->from) && isset($request->to)){



             $from=strval($request->from);

             $to=strval($request->to);


           $dateFrom=strtotime($from." 00:00:00");

            $fromTime=date("Y-m-d H:i:s",$dateFrom);



            $dateTo=strtotime($to." 23:59:00");

            $toTime=date("Y-m-d H:i:s",$dateTo);


            $contenido=venta::with('user')->with('estado')->with('forma_pago')->whereBetween('created_at', [$fromTime, $toTime])->paginate(6);




           // ["2019-02-21 00:00:00", "2019-02-22 23:59:00"]

            


       }else{



             $contenido=venta::with('user')->with('estado')->with('forma_pago')->orderBy('created_at','desc')->paginate(7);

          
       }

       $estados=estado::all();


       


       // return $contenido;
        return view('cms.ventas',compact('contenido','estados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       $array=json_decode($request->prendas,true);


       $precioTotal=0;

        for ($i=0; $i <count($array) ; $i++){ 
         
          $idprenda=$array[$i]['id'];
          $cantidad=$array[$i]['cantidad'];
          $prenda = prenda::where('id',$idprenda)->first();
          $precioTotal += intval($prenda['precio']) *  intval($cantidad);

        }


      $venta = new venta;
      $venta->user_id = Auth::id();
      $venta->total =intval($precioTotal);
      $venta->cenv = 0; 
      $venta->totalp = intval($precioTotal); 
      $venta->forma_pago_id=3;
      $venta->estado_id=3;
      $venta->save();

      

       for ($i=0; $i <count($array) ; $i++) { 

          $idprenda=$array[$i]['id'];
          $talla=$array[$i]['keytalla'];
          $color=$array[$i]['color'];
          $cantidad=$array[$i]['cantidad'];

           
          color::where('prenda_id',$idprenda)
                 ->where('color',$color)
                 ->where('talla_id',$talla)
                 ->update([ // increment
                    'stock' => \DB::raw( "stock -${cantidad}"), // decrement
                   ]);



         $prenda2= prenda::where('id',$idprenda)->first();


         $detalle= new detail;

         $detalle->venta_id=$venta->id;
         $detalle->prenda_id=$idprenda;
         $detalle->talla=$array[$i]['talla'];
         $detalle->color=$array[$i]['color'];
         $detalle->image=$array[$i]['imagen'];
         $detalle->puni=$prenda2['precio'];
         $detalle->cant=$cantidad;
         $detalle->total=intval($prenda2['precio'])* intval($cantidad);


         $detalle->save();





      }




      $direccion = new direccion;


      $direccion->venta_id=$venta->id;
      $direccion->nombre=$request->nombre;
      $direccion->apellido=$request->apellido;
      $direccion->distrito=$request->distrito;
      $direccion->direccion=$request->direccion;
      $direccion->referencia=$request->referencia;
      $direccion->telf=$request->telefono1;
      $direccion->telfopc=$request->telefono2;

      $direccion->save();


      $rpta=collect(["ok"=>true,"msj"=>"venta exitosa"]);


      
      
     return  $rpta;      

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $venta=venta::where('id',$id)
         ->update(['estado_id' => $request->idEstado]);


          return $venta;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


      $detalle=detail::where('venta_id',$id)->get();


          for ($i=0; $i <count($detalle) ; $i++) { 

                $color=$detalle[$i]->color;
                $tallan=$detalle[$i]->talla;
                $idPrenda=$detalle[$i]->prenda_id;
                $cantidad=$detalle[$i]->cant;

                $talla=talla::where('talla',$tallan)->first();


                  $idTalla=$talla->id;


                    color::where('prenda_id',$idPrenda)
                     ->where('color',$color)
                     ->where('talla_id',$idTalla)
                     ->update([ // increment
                        'stock' => \DB::raw( "stock +${cantidad}"), // decrement
                       ]);


          }








          detail::where('venta_id',$id)->delete();


           venta::where('id',$id)->delete();



      



      

    return back();


      
        
    }
}
