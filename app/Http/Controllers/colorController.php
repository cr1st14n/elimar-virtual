<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\color;

class colorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         $color= color::all();


          return $color;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $id=$request->input('prenda_id');

       $color=$request->input('color');

       $talla=$request->input('talla_id');





        $buscando= color::where(['talla_id'=>$talla,
                                  'color'=>$color,
                                   'prenda_id'=>$id

                               ])->first();



        if($buscando){

           $buscando->update($request->all());

             // return $buscando;
        }else{

           color::create($request->all());
        }


        $buscando2=color::where('prenda_id',$id)->with('talla')->get();

        return $buscando2;


   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $colores=color::where('prenda_id',$id)->with('talla')->get();

       return $colores;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $idprenda=$request->input('prenda_id');
        $talla=$request->input('talla_id');
        $color=$request->input('color');


        $rpta=color::where(['talla_id'=>$talla,
                                  'color'=>$color,
                                   'prenda_id'=>$idprenda

                               ])->delete();



    $rpta=color::where('prenda_id',$idprenda)->with('talla')->get();


    return $request->all();
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // color::where('id',$id)->delete();

        


    }
}
