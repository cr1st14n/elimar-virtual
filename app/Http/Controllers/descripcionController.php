<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\prenda;

use App\color;

use App\coment;

use App\User;

class descripcionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id,$color)
    {

//variables para la vista

        $prenda= prenda::where('id',$id)->with('image')->get();



        $stock=color::where('prenda_id',$id)->where('color',$color)->with('talla')->get();


          $comentarios=coment::where('prenda_id',$id)->with('user')->paginate(2);

          $total=$comentarios->lastPage();

          $colleccion=collect($prenda[0]->image)->groupBy('color');       

         $principal = $colleccion[$color];


         // return $principal;


          $result = array();

          $bidimensional=array();

          $aux = 0;

        foreach($colleccion as $d => $key){

            foreach ($key as $listar) {

                

                  $result[] = $listar;
                

                  
            }

              $bidimensional[]= $result;

              $result=[];


        }

        // return $comentarios;



         return view('prendaDetails',['bidimensional'=>$bidimensional,'listaStock'=>$stock,'prenda'=>$prenda, 'principal'=>$principal,'comentarios'=>$comentarios,'total'=>$total] ) ;
      

    }
}
