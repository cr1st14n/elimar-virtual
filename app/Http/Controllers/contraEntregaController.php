<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class contraEntregaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct(){


        // $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth');


    }

    public function __invoke(Request $request)
    {



        return view('contraEntrega');

    }
}
