<?php

namespace App\Http\Controllers;

use App\talla;

use Illuminate\Http\Request;

// // use Request; 
// use Illuminate\Support\Facades\Request;

class tallaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(){

      $this->middleware('onlyAdmin',['except'=>['show']]);

        $this->middleware('auth',['except'=>['show']]);


 }


    public function index()
    {
        

        $talla=talla::all();





           
            return view('cms.talla',compact('talla'));
        



       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $talla=$request->input('talla');

        $buscando= talla::where('talla',$talla)->first();


        if($buscando){

            $buscando->state=true;

               $buscando->save();

        }else{


            talla::create($request->all());

    

        }

           return back();
        
       


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
         

         $resultado=talla::find($id);      
    

        return $resultado;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateTalla=talla::find($id);

          $updateTalla->update($request->all());


          return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $talla= talla::find($id);

      $talla->state=false;

      $talla->save();

      return back();
    }
}
