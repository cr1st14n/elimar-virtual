<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\coment;

use Illuminate\Support\Facades\Auth;

class comentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $comentarios=new coment;

        $comentarios->prenda_id= $request->idprenda;
        $comentarios->user_id= intval(Auth::id());
        $comentarios->comentario=$request->comentario;
        $comentarios->venta=intval($request->idventa);
        $comentarios->save();
       $rpta= collect(['ok'=>'true']);

       return $rpta;
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
