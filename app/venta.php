<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class venta extends Model
{
    //


    public function forma_pago(){

    	return $this->belongsTo('App\forma_pago');
    }

    public function estado(){

    	return $this->belongsTo('App\estado');
    }

    public function User(){

    	return $this->belongsTo('App\User');
    }

   
}
