<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estado extends Model
{
     protected $fillable=['estado','descripcion'];
    protected $attributes = [
	'state' => true
];}
