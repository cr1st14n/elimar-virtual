<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class forma_pago extends Model
{
   protected $fillable=['fpago','descripcion'];
    protected $attributes = [
	'state' => true
];
}
