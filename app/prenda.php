<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prenda extends Model
{
    //

    protected $fillable=['nombre','tipo_id','descripcion','precio'];


    public function tipo(){

        return $this->belongsTo('App\tipo');

    }


    public function image(){

    	return $this->hasMany('App\image');
    }

    public function coment(){

    	return $this->hasMany('App\coment');


    }


}
