<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class talla extends Model
{
    protected $fillable=['talla','descripcion'];
    protected $attributes = [
	'state' => true
];
}
