<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::resource('/','indexController');


Route::get('/direccion/{id}','direccionController');


Route::get('/galeria/{id?}','prendasController@mostrar')->name('prendas');

// Route::view('/galeria','prendas')->name('prendas');

// Route::view('/prendaDescripcion','prendaDetails')->name('prendaDetails');

Route::get('/prendaDescripcion/{id?}/{color?}','descripcionController')->name('prendaDescripcion');


Route::get('/contraEntrega','contraEntregaController')->name('contraEntrega')->middleware('verified');


Route::get('/obtenerComentarios/{id?}','getComentariosController')->name('getComentarios');
// Route::get('/contraEntrega')

Route::view('/cart','carrito')->name('carrito');




Route::resource('/resenas','resenasController')->middleware('verified');


Route::get('/compras','comprasController')->name('compras')->middleware('verified');


Route::get('/detalle/{id?}','comprasController@detalles')->name('detalle');





Route::view('/pagoTarjeta','pagoTarjeta')->name('pagoTarjeta');


Route::view('/transferencia','transferencia')->name('transferencia');





// Route::get('/tipoPrenda',function(){
// 	return view('cms.formaPago');
// });


Route::resource('/comentarios','comentariosController');

Route::resource('/talla','tallaController');


Route::resource('/tipoPrenda','tipoPrendaController');


Route::resource('/formaPago','formaPagoController');

Route::resource('/estadoVenta','estadoVentaController');

Route::resource('/ventas','ventasController');





Route::resource('/prenda','prendaController');


Route::resource('/color','colorController');

Route::resource('/imagen','imagenController');




// RUTAS DE CONSULTA

Auth::routes(['verify'=> true]);


Route::get('/dataTalla','dataTallaController');


Route::get('/dataTipo','dataTipoController');


Route::get('/dataColor/{id}','dataColorController');



//FIN RUTAS DE CONSULTA


// Route::view('/tipoPrenda','cms.tipoPrenda')->name('cms.tipoPrenda');

// Route::view('/formaPago','cms.formaPago')->name('cms.formaPago');

// Route::view('/estadosVenta','cms.estadosVenta')->name('cms.estadoVenta');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');




Route::get('/ventasDetalle/{id}','detallesController');
